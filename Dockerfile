FROM openjdk:8-jdk-alpine
EXPOSE 8080
ARG JAR_FILE=target/mobile-app-ws-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} mobile-app-ws.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/mobile-app-ws.jar"]