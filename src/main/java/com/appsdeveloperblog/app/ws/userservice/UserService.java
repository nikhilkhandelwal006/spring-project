package com.appsdeveloperblog.app.ws.userservice;

import com.appsdeveloperblog.app.ws.ui.model.request.UserDetailsModelRequest;
import com.appsdeveloperblog.app.ws.ui.model.response.userRest;

public interface UserService {
	userRest createuser(UserDetailsModelRequest userDetails);

}
