package com.appsdeveloperblog.app.ws.userservice.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.appsdeveloperblog.app.ws.shared.Utils;
import com.appsdeveloperblog.app.ws.ui.model.request.UserDetailsModelRequest;
import com.appsdeveloperblog.app.ws.ui.model.response.userRest;
import com.appsdeveloperblog.app.ws.userservice.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	Utils utils;
	public UserServiceImpl() {}
	
	@Autowired
	public UserServiceImpl(Utils utils) {
		
		this.utils= utils;
	}
	

	Map<String,userRest> users;
	@Override
	public userRest createuser(UserDetailsModelRequest userDetails) {
		userRest returnValue = new userRest();
		returnValue.setFirstName(userDetails.getFirstname());
		returnValue.setLastName(userDetails.getLastname());
		returnValue.setEmail(userDetails.getEmail());
		String userId = utils.genereateuserId();
		returnValue.setUserId(userId);
		
		if (users == null) users = new HashMap<>();
		users.put(userId, returnValue);
		return returnValue;
	}

}
