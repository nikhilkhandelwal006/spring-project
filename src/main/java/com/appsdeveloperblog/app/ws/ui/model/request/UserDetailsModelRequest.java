package com.appsdeveloperblog.app.ws.ui.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDetailsModelRequest {

	@NotNull(message= "first name cannot be null")
	@Size(min=2, message= "first name must not be less than 2 characters")
	private String firstname;
	@Size(min=2, message= "last name must not be less than 2 characters")
	@NotNull(message= "last name cannot be null")
	private String lastname;
	@Email
	@NotNull(message= "Email name cannot be null")
	private String email;
	@NotNull(message= "password name cannot be null")
	@Size(min=8,max=16, message= "password must be equal or greter than 8 character and less than 16 characters")
	private String password;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
