package com.appsdeveloperblog.app.ws.ui.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDetailsModelRequest {

	@NotNull(message = "first name cannot be null")
	@Size(min = 2, message = "first name must not be less than 2 characters")
	private String firstname;
	@Size(min = 2, message = "last name must not be less than 2 characters")
	@NotNull(message = "last name cannot be null")
	private String lastname;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

}
